//    uniCenta oPOS  - Touch Friendly Point Of Sale
//    Copyright (c) 2009-2016 uniCenta
//    https://unicenta.com
//
//    This file is part of uniCenta oPOS
//
//    uniCenta oPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   uniCenta oPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with uniCenta oPOS.  If not, see <http://www.gnu.org/licenses/>.

package com.GrowPOS.pos.backup;



import com.unicenta.pos.transfer.*;
import com.openbravo.data.loader.BatchSentence;
import com.openbravo.data.loader.BatchSentenceResource;
import com.openbravo.data.loader.Session;
import com.openbravo.pos.config.PanelConfig;
import com.openbravo.pos.forms.*;
import com.openbravo.pos.util.AltEncrypter;
import com.openbravo.pos.util.DirectoryEvent;

import java.awt.HeadlessException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.regex.Matcher;
import javax.swing.*;

import com.openbravo.basic.BasicException;
import com.openbravo.data.gui.JMessageDialog;
import com.openbravo.data.gui.MessageInf;
import com.openbravo.data.user.DirtyManager;
import com.openbravo.pos.forms.AppLocal;
import com.openbravo.pos.forms.AppView;
import com.openbravo.pos.util.OSValidator;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ProcessBuilder.Redirect;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.swing.JPanel;


/**
 *
 * @author JG uniCenta
 */
public final class Backup extends JPanel implements JPanelView {

    private DirtyManager dirty = new DirtyManager();
    private AppConfig config;
    private AppProperties m_props;
    private List<PanelConfig> m_panelconfig;
    
    private Connection con_source;
    private Connection con_target;

    private String sDB_source;
    private String sDB_target;
    
    private Session session_source;
    private Session session_target;

    private ResultSet rs;

    private Statement stmt_source;
    private Statement stmt_target;
    private String SQL;
    private PreparedStatement pstmt;
    
    private String source_version;

    private String ticketsnum;
    private String ticketsnumRefund;
    private String ticketsnumPayment;

    private String targetCreate = "";
    private String targetFKadd = "";
    private String targetFKdrop = "";    
    
    ArrayList<String> stringList = new ArrayList<>();
    
    public String strOut = "";
        

    /**
     * Creates new form JPaneldbTransfer
     * @param oApp
     */
    public Backup(AppView oApp) {
        this(oApp.getProperties());
    }

    /**
     *
     * @param props
     */
    public Backup(AppProperties props) {

        initComponents();

        config = new AppConfig(props.getConfigFile());
        m_props = props;
        m_panelconfig = new ArrayList<>();
        config.load();
        
        m_panelconfig.stream().forEach((c) -> {
            c.loadProperties(config);
        });

        jtxtDbDriverLib.getDocument().addDocumentListener(dirty);
        jtxtDbDriver.getDocument().addDocumentListener(dirty);
        jtxtDbURL.getDocument().addDocumentListener(dirty);
        txtDbPass.getDocument().addDocumentListener(dirty);
        txtDbUser.getDocument().addDocumentListener(dirty);
        jtxtDbLocation.getDocument().addDocumentListener(dirty);
        jtxtOutFile.getDocument().addDocumentListener(dirty);
        txtOut.getDocument().addDocumentListener(dirty);
        
        jbtnDbDriverLib.addActionListener(new DirectoryEvent(jtxtDbDriverLib));
        jbtnDbLocation.addActionListener(new DirectoryEvent(jtxtDbLocation));
        //jbtnOutFile.addActionListener(new DirectoryEvent(jtxtOutFile));

        cbSource.addActionListener(dirty);
        
        cbSource.addItem("Derby");
        cbSource.addItem("MySQL");
        cbSource.addItem("PostgreSQL");
        
        stringList.add("Transfer Ready..." + "\n");
        txtOut.setText(stringList.get(0));

        webPBar.setIndeterminate ( true );
        webPBar.setStringPainted ( true );
        webPBar.setString ( "Waiting..." );
        webPBar.setVisible(false);
        
        jbtnBackup.setEnabled(false);
        jbtnExit.setVisible(false);
       
    }

    /**
     *
     * @return
     */
    @Override
    public JComponent getComponent() {
        return this;
    }

    /**
     *
     * @return
     */
    @Override
    public String getTitle() {
        return AppLocal.getIntString("Menu.Backup.Title");
    }

    /**
     *
     * @return
     */
    public Boolean getSource() {

        String db_user2 = txtDbUser.getText();
        String db_url2 = jtxtDbURL.getText();
        char[] pass = txtDbPass.getPassword();
        String db_password2 = new String(pass);

        Properties connectionProps = new Properties();
        connectionProps.put("user", db_user2);
        connectionProps.put("password", db_password2);
        
        try {
            Class.forName(jtxtDbDriver.getText());

            ClassLoader cloader = new URLClassLoader(
                    new URL[]{
                        new File(jtxtDbDriverLib.getText()).toURI().toURL()
                    });
            DriverManager.registerDriver(
                    new DriverWrapper((Driver) Class.forName(jtxtDbDriver.getText(), 
                            true, cloader).newInstance()));
            con_source = (Connection) DriverManager.getConnection(
                    db_url2, db_user2, db_password2);

            session_source = new Session(db_url2, db_user2, db_password2);
            sDB_source = con_source.getMetaData().getDatabaseProductName();          

            txtOut.append("Connected to Source OK" + "\n");
            jbtnBackup.setEnabled(true);            
            
            return (true);
            
        } catch (ClassNotFoundException 
                | MalformedURLException 
                | InstantiationException 
                | IllegalAccessException 
                | SQLException e) {
            
            JMessageDialog.showMessage(this, 
                    new MessageInf(MessageInf.SGN_DANGER, 
                            AppLocal.getIntString("database.UnableToConnect"), 
                            e));
            return (false);
        }

    }
    
    /**
     *
     * @throws BasicException
     */
    @Override
    public void activate() throws BasicException {

    
        String db_user = (m_props.getProperty("db.user"));
        String db_url = (m_props.getProperty("db.URL"));
        String db_password = (m_props.getProperty("db.password"));

        if (db_user != null 
                && db_password != null 
                && db_password.startsWith("crypt:")) {

            AltEncrypter cypher = new AltEncrypter("cypherkey" + db_user);
            db_password = cypher.decrypt(db_password.substring(6));
        }

        try {
            session_target = AppViewConnection.createSession(m_props);            
            con_target  = DriverManager.getConnection(db_url, db_user, db_password);
            sDB_target = con_target.getMetaData().getDatabaseProductName();
            jlblSource.setText(con_target.getCatalog());          
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String fileName = (dateFormat.format(date)).toString()+"-"+jlblSource.getText()+".sql";
            jtxtOutFile.setText(new File(new File(System.getProperty("user.home")), fileName).toString());   
            
        } catch (BasicException | SQLException e) {
            JMessageDialog.showMessage(this, 
                    new MessageInf(MessageInf.SGN_DANGER, 
                            AppLocal.getIntString("database.UnableToConnect"), 
                            e));
        }
    }
    

    /**
     *
     * @return
     */
    @Override
    public boolean deactivate() {
        return (true);
    }
    
    
    public void doBackup() throws InterruptedException, IOException {

        webPBar.setString ( "Starting..." );
        webPBar.setVisible(true);
         
        String Dbtname = "";
        
        if (getSource()) {          
                    txtOut.setVisible(true);
                    txtOut.append("Backup Started..." + "\n");
                    
                    jbtnBackup.setEnabled(false);
                    int iend = jtxtDbURL.getText().lastIndexOf("/");
                    String database;
                    database= jtxtDbURL.getText().substring(iend+1);
                    String dump = jtxtDbLocation.getText()+" -u"+txtDbUser.getText()+" -p"+String.valueOf(txtDbPass.getPassword())+" "+database+" >"+jtxtOutFile.getText();
                    String os = new OSValidator().getOS();
                    String[] cmdarray;
                    if(os.equals("w")){
                        cmdarray= new String[] {"cmd.exe","/c", dump};
                    } else{
                       cmdarray = new String[] {"/bin/sh","-c", dump}; 
                    }
                    
                    
                    Process p = Runtime.getRuntime().exec(cmdarray);
                    //Wait for the command to complete, and check if the exit value was 0 (success)
                    if(p.waitFor()==0) {
                        InputStream inputStream = p.getInputStream();
                        byte[] buffer = new byte[inputStream.available()];
                        inputStream.read(buffer);

                        String str = new String(buffer);
                        System.out.println(str);
                        txtOut.append("Backup Finish Successfull..." + "\n");
                        webPBar.setString ( "Finish..." );  
                    }
                    else {
                        InputStream errorStream = p.getErrorStream();
                        byte[] buffer = new byte[errorStream.available()];
                        errorStream.read(buffer);

                        String str = new String(buffer);
                        System.out.println(str);
                        txtOut.append("An error occurred during the process..." + "\n");
                        webPBar.setString ( "Failed..." );  

                    }

            session_source.close();
            
        }
    }
       
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel5 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        webPanel2 = new com.alee.laf.panel.WebPanel();
        cbSource = new com.alee.laf.combobox.WebComboBox();
        jtxtDbDriverLib = new com.alee.laf.text.WebTextField();
        jbtnDbDriverLib = new javax.swing.JButton();
        jtxtDbDriver = new com.alee.laf.text.WebTextField();
        jtxtDbURL = new com.alee.laf.text.WebTextField();
        jlblVersion = new javax.swing.JLabel();
        txtDbUser = new com.alee.laf.text.WebTextField();
        txtDbPass = new com.alee.laf.text.WebPasswordField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtOut = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        jlblSource = new javax.swing.JLabel();
        jbtnExit = new javax.swing.JButton();
        jtxtDbName = new com.alee.laf.text.WebTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jbtnTest = new javax.swing.JButton();
        jbtnBackup = new javax.swing.JButton();
        webPBar = new com.alee.laf.progressbar.WebProgressBar();
        jLabel11 = new javax.swing.JLabel();
        jbtnDbLocation = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jbtnOutFile = new javax.swing.JButton();
        jtxtDbLocation = new com.alee.laf.text.WebTextField();
        jtxtOutFile = new com.alee.laf.text.WebTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        setPreferredSize(new java.awt.Dimension(900, 425));

        jLabel5.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(102, 102, 102));
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("pos_messages"); // NOI18N
        jLabel5.setText(bundle.getString("label.DbType")); // NOI18N
        jLabel5.setMaximumSize(new java.awt.Dimension(150, 30));
        jLabel5.setMinimumSize(new java.awt.Dimension(150, 30));
        jLabel5.setPreferredSize(new java.awt.Dimension(160, 30));

        jLabel18.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(102, 102, 102));
        jLabel18.setText(AppLocal.getIntString("label.dbdriverlib")); // NOI18N
        jLabel18.setMaximumSize(new java.awt.Dimension(150, 30));
        jLabel18.setMinimumSize(new java.awt.Dimension(150, 30));
        jLabel18.setPreferredSize(new java.awt.Dimension(160, 30));

        jLabel1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(102, 102, 102));
        jLabel1.setText(AppLocal.getIntString("label.DbDriver")); // NOI18N
        jLabel1.setMaximumSize(new java.awt.Dimension(150, 30));
        jLabel1.setMinimumSize(new java.awt.Dimension(150, 30));
        jLabel1.setPreferredSize(new java.awt.Dimension(160, 30));

        jLabel2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(102, 102, 102));
        jLabel2.setText("URL");
        jLabel2.setMaximumSize(new java.awt.Dimension(150, 30));
        jLabel2.setMinimumSize(new java.awt.Dimension(150, 30));
        jLabel2.setPreferredSize(new java.awt.Dimension(160, 30));

        jLabel7.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(102, 102, 102));
        jLabel7.setText("DB Version");
        jLabel7.setMaximumSize(new java.awt.Dimension(150, 30));
        jLabel7.setMinimumSize(new java.awt.Dimension(150, 30));
        jLabel7.setPreferredSize(new java.awt.Dimension(160, 30));

        jLabel3.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(102, 102, 102));
        jLabel3.setText(AppLocal.getIntString("label.DbUser")); // NOI18N
        jLabel3.setMaximumSize(new java.awt.Dimension(150, 30));
        jLabel3.setMinimumSize(new java.awt.Dimension(150, 30));
        jLabel3.setPreferredSize(new java.awt.Dimension(160, 30));

        jLabel4.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(102, 102, 102));
        jLabel4.setText(AppLocal.getIntString("label.DbPassword")); // NOI18N
        jLabel4.setMaximumSize(new java.awt.Dimension(150, 30));
        jLabel4.setMinimumSize(new java.awt.Dimension(150, 30));
        jLabel4.setPreferredSize(new java.awt.Dimension(160, 30));

        jLabel6.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("BACKUP DATABASE:");
        jLabel6.setPreferredSize(new java.awt.Dimension(150, 30));

        webPanel2.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout webPanel2Layout = new javax.swing.GroupLayout(webPanel2);
        webPanel2.setLayout(webPanel2Layout);
        webPanel2Layout.setHorizontalGroup(
            webPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 270, Short.MAX_VALUE)
        );
        webPanel2Layout.setVerticalGroup(
            webPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 51, Short.MAX_VALUE)
        );

        cbSource.setForeground(new java.awt.Color(51, 51, 51));
        cbSource.setToolTipText(bundle.getString("tooltip.backupfromdb"));
        cbSource.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cbSource.setPreferredSize(new java.awt.Dimension(150, 30));
        cbSource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbSourceActionPerformed(evt);
            }
        });

        jtxtDbDriverLib.setForeground(new java.awt.Color(51, 51, 51));
        jtxtDbDriverLib.setToolTipText(bundle.getString("tootltip.transferlib")); // NOI18N
        jtxtDbDriverLib.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jtxtDbDriverLib.setPreferredSize(new java.awt.Dimension(300, 30));

        jbtnDbDriverLib.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/openbravo/images/fileopen.png"))); // NOI18N
        jbtnDbDriverLib.setToolTipText(bundle.getString("tooltip.openfile")); // NOI18N
        jbtnDbDriverLib.setMaximumSize(new java.awt.Dimension(64, 32));
        jbtnDbDriverLib.setMinimumSize(new java.awt.Dimension(64, 32));
        jbtnDbDriverLib.setPreferredSize(new java.awt.Dimension(60, 30));
        jbtnDbDriverLib.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnDbDriverLibActionPerformed(evt);
            }
        });

        jtxtDbDriver.setForeground(new java.awt.Color(51, 51, 51));
        jtxtDbDriver.setToolTipText(bundle.getString("tootltip.transferclass")); // NOI18N
        jtxtDbDriver.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jtxtDbDriver.setPreferredSize(new java.awt.Dimension(300, 30));

        jtxtDbURL.setForeground(new java.awt.Color(51, 51, 51));
        jtxtDbURL.setToolTipText(bundle.getString("tootltip.transferdbname")); // NOI18N
        jtxtDbURL.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jtxtDbURL.setPreferredSize(new java.awt.Dimension(300, 30));

        jlblVersion.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        jlblVersion.setForeground(new java.awt.Color(0, 204, 255));
        jlblVersion.setToolTipText(bundle.getString("tooltip.transferdbversion")); // NOI18N
        jlblVersion.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 204, 255)));
        jlblVersion.setMaximumSize(new java.awt.Dimension(150, 30));
        jlblVersion.setMinimumSize(new java.awt.Dimension(150, 30));
        jlblVersion.setPreferredSize(new java.awt.Dimension(100, 30));

        txtDbUser.setForeground(new java.awt.Color(51, 51, 51));
        txtDbUser.setToolTipText(bundle.getString("tooltip.dbuser")); // NOI18N
        txtDbUser.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        txtDbUser.setPreferredSize(new java.awt.Dimension(300, 30));

        txtDbPass.setForeground(new java.awt.Color(51, 51, 51));
        txtDbPass.setToolTipText(bundle.getString("tooltip.dbpassword")); // NOI18N
        txtDbPass.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        txtDbPass.setPreferredSize(new java.awt.Dimension(300, 30));

        jScrollPane1.setBorder(null);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jScrollPane1.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        jScrollPane1.setHorizontalScrollBar(null);
        jScrollPane1.setPreferredSize(new java.awt.Dimension(200, 350));

        txtOut.setColumns(20);
        txtOut.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtOut.setForeground(new java.awt.Color(0, 153, 255));
        txtOut.setRows(5);
        txtOut.setToolTipText(bundle.getString("tooltip.transfertxtout")); // NOI18N
        txtOut.setBorder(null);
        jScrollPane1.setViewportView(txtOut);

        jLabel8.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 153, 255));
        jLabel8.setText("PROGRESS");
        jLabel8.setPreferredSize(new java.awt.Dimension(150, 30));

        jlblSource.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jlblSource.setForeground(new java.awt.Color(0, 153, 255));
        jlblSource.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlblSource.setPreferredSize(new java.awt.Dimension(150, 30));

        jbtnExit.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jbtnExit.setText(AppLocal.getIntString("Button.Exit")); // NOI18N
        jbtnExit.setToolTipText(bundle.getString("tooltip.exit")); // NOI18N
        jbtnExit.setMaximumSize(new java.awt.Dimension(70, 33));
        jbtnExit.setMinimumSize(new java.awt.Dimension(70, 33));
        jbtnExit.setPreferredSize(new java.awt.Dimension(80, 45));
        jbtnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnExitActionPerformed(evt);
            }
        });

        jtxtDbName.setForeground(new java.awt.Color(51, 51, 51));
        jtxtDbName.setToolTipText(bundle.getString("tooltip.dbname")); // NOI18N
        jtxtDbName.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jtxtDbName.setPreferredSize(new java.awt.Dimension(300, 30));
        jtxtDbName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtxtDbNameFocusLost(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(102, 102, 102));
        jLabel9.setText(AppLocal.getIntString("label.DbUser")); // NOI18N
        jLabel9.setMaximumSize(new java.awt.Dimension(150, 30));
        jLabel9.setMinimumSize(new java.awt.Dimension(150, 30));
        jLabel9.setPreferredSize(new java.awt.Dimension(160, 30));

        jLabel10.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(102, 102, 102));
        jLabel10.setText(AppLocal.getIntString("label.DbName")); // NOI18N
        jLabel10.setMaximumSize(new java.awt.Dimension(150, 30));
        jLabel10.setMinimumSize(new java.awt.Dimension(150, 30));
        jLabel10.setPreferredSize(new java.awt.Dimension(160, 30));

        jbtnTest.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jbtnTest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/openbravo/images/database.png"))); // NOI18N
        jbtnTest.setText(bundle.getString("Button.Test")); // NOI18N
        jbtnTest.setToolTipText(bundle.getString("tooltip.dbbackup"));
        jbtnTest.setActionCommand(bundle.getString("Button.Test")); // NOI18N
        jbtnTest.setPreferredSize(new java.awt.Dimension(160, 45));
        jbtnTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnTestjButtonTestConnectionActionPerformed(evt);
            }
        });

        jbtnBackup.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jbtnBackup.setText(AppLocal.getIntString("button.backup"));
        jbtnBackup.setToolTipText(bundle.getString("tooltip.backupdb"));
        jbtnBackup.setMaximumSize(new java.awt.Dimension(70, 33));
        jbtnBackup.setMinimumSize(new java.awt.Dimension(70, 33));
        jbtnBackup.setPreferredSize(new java.awt.Dimension(160, 45));
        jbtnBackup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnBackupActionPerformed(evt);
            }
        });

        webPBar.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        webPBar.setPreferredSize(new java.awt.Dimension(240, 30));

        jLabel11.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(102, 102, 102));
        jLabel11.setText("DB Fisical Location");//AppLocal.getIntString("label.DbPassword"));
    jLabel11.setMaximumSize(new java.awt.Dimension(150, 30));
    jLabel11.setMinimumSize(new java.awt.Dimension(150, 30));
    jLabel11.setPreferredSize(new java.awt.Dimension(160, 30));

    jbtnDbLocation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/openbravo/images/fileopen.png"))); // NOI18N
    jbtnDbLocation.setToolTipText(bundle.getString("tooltip.openfile")); // NOI18N
    jbtnDbLocation.setMaximumSize(new java.awt.Dimension(64, 32));
    jbtnDbLocation.setMinimumSize(new java.awt.Dimension(64, 32));
    jbtnDbLocation.setPreferredSize(new java.awt.Dimension(60, 30));
    jbtnDbLocation.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jbtnDbLocationActionPerformed(evt);
        }
    });

    jLabel12.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
    jLabel12.setForeground(new java.awt.Color(102, 102, 102));
    jLabel12.setText("Output file");//AppLocal.getIntString("label.DbPassword"));
    jLabel12.setMaximumSize(new java.awt.Dimension(150, 30));
    jLabel12.setMinimumSize(new java.awt.Dimension(150, 30));
    jLabel12.setPreferredSize(new java.awt.Dimension(160, 30));

    jbtnOutFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/openbravo/images/fileopen.png"))); // NOI18N
    jbtnOutFile.setToolTipText(bundle.getString("tooltip.openfile")); // NOI18N
    jbtnOutFile.setMaximumSize(new java.awt.Dimension(64, 32));
    jbtnOutFile.setMinimumSize(new java.awt.Dimension(64, 32));
    jbtnOutFile.setPreferredSize(new java.awt.Dimension(60, 30));
    jbtnOutFile.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jbtnOutFileActionPerformed(evt);
        }
    });

    jtxtDbLocation.setForeground(new java.awt.Color(51, 51, 51));
    jtxtDbLocation.setToolTipText(bundle.getString("tootltip.transferdbname")); // NOI18N
    jtxtDbLocation.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
    jtxtDbLocation.setPreferredSize(new java.awt.Dimension(300, 30));

    jtxtOutFile.setForeground(new java.awt.Color(51, 51, 51));
    jtxtOutFile.setToolTipText(bundle.getString("tootltip.transferdbname")); // NOI18N
    jtxtOutFile.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
    jtxtOutFile.setPreferredSize(new java.awt.Dimension(300, 30));

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel9, 0, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jtxtDbName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(cbSource, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtDbUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(160, 160, 160)
                                    .addComponent(jbtnTest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jbtnBackup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jbtnExit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtDbPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jlblVersion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jtxtDbLocation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jtxtOutFile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jbtnDbLocation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jbtnOutFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGap(78, 78, 78))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jlblSource, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(webPBar, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel18, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jtxtDbDriverLib, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jbtnDbDriverLib, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jtxtDbDriver, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jtxtDbURL, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(webPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap())
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlblSource, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(webPBar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(webPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cbSource, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jtxtDbName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jtxtDbDriverLib, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jbtnDbDriverLib, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jtxtDbDriver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jtxtDbURL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtDbUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtDbPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jtxtDbLocation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jbtnDbLocation, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(10, 10, 10))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(jlblVersion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(6, 6, 6)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jbtnTest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jbtnBackup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jbtnExit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(20, 20, 20))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jtxtOutFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jbtnOutFile, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
    );

    jLabel1.getAccessibleContext().setAccessibleName("DBDriver");
    }// </editor-fold>//GEN-END:initComponents

    private void jbtnBackupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnBackupActionPerformed
// csvimport & sharedtickets are transient tables and so not transferred
// v4 tables and later are not transferred
        if (JOptionPane.showConfirmDialog(this, 
                AppLocal.getIntString("message.transfer"), 
                AppLocal.getIntString("message.transfertitle"), 
                JOptionPane.YES_NO_OPTION, 
                JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {          

            SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
            
                @Override
                protected Void doInBackground() throws Exception {
                    webPBar.setString ( "Starting..." );                
                    doBackup();       
                    return null;
                }
                
            };
            
            worker.execute();
            
        }

    }//GEN-LAST:event_jbtnBackupActionPerformed

    private void jbtnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnExitActionPerformed

        session_source.close();

        deactivate();
        
    }//GEN-LAST:event_jbtnExitActionPerformed

    private void jbtnTestjButtonTestConnectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnTestjButtonTestConnectionActionPerformed

        try {
            String driverlib = jtxtDbDriverLib.getText();
            String driver = jtxtDbDriver.getText();
            String url = jtxtDbURL.getText();
            String user = txtDbUser.getText();
            String password = new String(txtDbPass.getPassword());
            String dbLocation = jtxtDbLocation.getText();
            String outfile = jtxtOutFile.getText();
            
            if (dbLocation == null || dbLocation.equals("")){
                JMessageDialog.showMessage(this, 
                    new MessageInf(MessageInf.SGN_WARNING, 
                    AppLocal.getIntString("Field error"),"No ha seleccionado el archivo"));
            } else {
            
                if(outfile == null || outfile.equals("")){
                    JMessageDialog.showMessage(this, 
                        new MessageInf(MessageInf.SGN_WARNING, 
                        AppLocal.getIntString("Field error"),"No ha añadido un archivo de salida"));
                } else {

                    ClassLoader cloader = new URLClassLoader(new URL[] {
                        new File(driverlib).toURI().toURL()
                    });

                    DriverManager.registerDriver(
                            new DriverWrapper((Driver) 
                                    Class.forName(driver, true, cloader).newInstance()));

                    Session session_source = new Session(url, user, password);
                    Connection connection = session_source.getConnection();
                    boolean isValid = (connection == null) 
                            ? false : connection.isValid(1000);

                    if (isValid) {
                        SQL = "SELECT * FROM applications";
                        Statement stmt = (Statement) connection.createStatement();
                        rs = stmt.executeQuery(SQL);            
                        rs.next();
                        jlblVersion.setText(rs.getString(3));

                        stringList.add("Version check... " + rs.getString(3) + "\n");
                        txtOut.setText(stringList.get(1));                

                        JOptionPane.showMessageDialog(this, 
                                AppLocal.getIntString("message.databaseconnectsuccess"), 
                                "Connection Test"
                                , JOptionPane.INFORMATION_MESSAGE);

                        jbtnBackup.setEnabled(true);

                    } else {
                        JMessageDialog.showMessage(this, 
                                new MessageInf(MessageInf.SGN_WARNING, "Connection Error"));
                    }
                }
            }
        } catch (InstantiationException 
                | IllegalAccessException 
                | MalformedURLException 
                | ClassNotFoundException e) {
            JMessageDialog.showMessage(this, 
                    new MessageInf(MessageInf.SGN_WARNING, 
                    AppLocal.getIntString("message.databasedrivererror"), e));
        
        } catch (SQLException e) {
            JMessageDialog.showMessage(this, 
                    new MessageInf(MessageInf.SGN_WARNING, 
                    AppLocal.getIntString("message.databaseconnectionerror"), e));
        
        } catch (Exception e) {
            JMessageDialog.showMessage(this, 
                    new MessageInf(MessageInf.SGN_WARNING, "Unknown exception", e));
        }
    }//GEN-LAST:event_jbtnTestjButtonTestConnectionActionPerformed

    private void cbSourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbSourceActionPerformed
        String dirname = System.getProperty("dirname.path");
        dirname = dirname == null ? "./" : dirname;
        if ("Derby".equals(cbSource.getSelectedItem())) {
            jtxtDbName.setText("GrowPOS-database");
            jtxtDbDriverLib.setText(new File(new File(dirname)
                    , "/lib/derby-10.10.2.0.jar").getAbsolutePath());
            jtxtDbDriver.setText("org.apache.derby.jdbc.EmbeddedDriver");
            jtxtDbURL.setText("jdbc:derby:" + new File(new File(System.getProperty("user.home"))
                    , "GrowPOS-database"));
            txtDbUser.setText("");
            txtDbPass.setText("");           
        } else if ("PostgreSQL".equals(cbSource.getSelectedItem())) {
            jtxtDbName.setText("GrowPOS");
            jtxtDbDriverLib.setText(new File(new File(dirname)
                    , "/lib/postgresql-9.4-1208.jdbc4.jar").getAbsolutePath());
            jtxtDbDriver.setText("org.postgresql.Driver");
            jtxtDbURL.setText("jdbc:postgresql://localhost:5432/GrowPOS");            
        } else {
            jtxtDbName.setText("GrowPOS");
            jtxtDbDriverLib.setText(new File(new File(dirname)
                    , "/lib/mysql-connector-java-5.1.34-bin.jar").getAbsolutePath());
            jtxtDbDriver.setText("com.mysql.jdbc.Driver");
            jtxtDbURL.setText("jdbc:mysql://localhost:3306/GrowPOS");
        }
        
    }//GEN-LAST:event_cbSourceActionPerformed

    private void jtxtDbNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtxtDbNameFocusLost
        jtxtDbURL.setText(jtxtDbName.getText());
        String dirname = System.getProperty("dirname.path");
                  
        if ("Derby".equals(cbSource.getSelectedItem())) {
            jtxtDbURL.setText("jdbc:derby:" + new File(new File(System.getProperty("user.home"))
                ,jtxtDbName.getText()));

        } else if ("PostgreSQL".equals(cbSource.getSelectedItem())) {
            jtxtDbURL.setText("jdbc:postgresql://localhost:5432/"
                + jtxtDbName.getText());            
        } else {
            jtxtDbURL.setText("jdbc:mysql://localhost:3306/"
                + jtxtDbName.getText()); 
        }        
        
    }//GEN-LAST:event_jtxtDbNameFocusLost

    private void jbtnDbDriverLibActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnDbDriverLibActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jbtnDbDriverLibActionPerformed

    private void jbtnDbLocationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnDbLocationActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jbtnDbLocationActionPerformed

    private void jbtnOutFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnOutFileActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setSelectedFile(new File(jtxtOutFile.getText()));
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
            jtxtOutFile.setText(chooser.getSelectedFile().toString());
        }
        else {
            System.out.println("No Selection ");
        }
    }//GEN-LAST:event_jbtnOutFileActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.alee.laf.combobox.WebComboBox cbSource;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jbtnBackup;
    private javax.swing.JButton jbtnDbDriverLib;
    private javax.swing.JButton jbtnDbLocation;
    private javax.swing.JButton jbtnExit;
    private javax.swing.JButton jbtnOutFile;
    private javax.swing.JButton jbtnTest;
    private javax.swing.JLabel jlblSource;
    private javax.swing.JLabel jlblVersion;
    private com.alee.laf.text.WebTextField jtxtDbDriver;
    private com.alee.laf.text.WebTextField jtxtDbDriverLib;
    private com.alee.laf.text.WebTextField jtxtDbLocation;
    private com.alee.laf.text.WebTextField jtxtDbName;
    private com.alee.laf.text.WebTextField jtxtDbURL;
    private com.alee.laf.text.WebTextField jtxtOutFile;
    private com.alee.laf.text.WebPasswordField txtDbPass;
    private com.alee.laf.text.WebTextField txtDbUser;
    private javax.swing.JTextArea txtOut;
    private com.alee.laf.progressbar.WebProgressBar webPBar;
    private com.alee.laf.panel.WebPanel webPanel2;
    // End of variables declaration//GEN-END:variables

}